const upperLetter=(txt)=>{
    let arr = txt.split(' ')
    let new_arr = []
    arr.forEach(element => {
        let letters = element.split('')
        letters[0] = letters[0].toUpperCase()
        let last = letters.join('')
        new_arr.push(last)
    });
    return new_arr.join(' ')
}

console.log(upperLetter('bill gates'))